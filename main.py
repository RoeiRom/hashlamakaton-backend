from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

import crud, models, schemas
from database import SessionLocal, engine
from utils import hash_password

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/login")
async def login(user_creds: schemas.UserCredntials, db: Session = Depends(get_db)):
    user = crud.get_user_by_username(db, user_creds.username)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    if user.hashed_password != hash_password(user_creds.password):
        raise HTTPException(status_code=403, detail="Forbidden")

    return { "user_id": user.id }


@app.post("/users/", response_model=schemas.User)
async def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    new_user = crud.create_user(db, user)
    return new_user


@app.get("/users/", response_model=List[schemas.User])
async def read_all_users(db: Session = Depends(get_db)):
    users = crud.get_all_users(db)
    return users


@app.get("/users/{user_id}", response_model=schemas.User)
async def read_user(user_id: int, db: Session = Depends(get_db)):
    user = crud.get_user(db, user_id)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return user


@app.post("/users/{user_id}/tags", response_model=schemas.User)
async def add_tags_to_user(user_id: int, tag_names: list[str], db: Session = Depends(get_db)):
    user = crud.get_user(db, user_id)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    crud.add_user_tags(db, user, tag_names)

    return user


@app.post("/tags/generate")
async def create_static_tags(db: Session = Depends(get_db)):
    crud.create_static_tags(db)


@app.post("/tags/", response_model=schemas.Tag)
async def create_tag(tag: schemas.Tag, db: Session = Depends(get_db)):
    return crud.create_tag(db, tag)


@app.get("/tags/")
async def read_all_tags(db: Session = Depends(get_db)):
    tags = crud.get_all_tags(db)

    categories_map = {}
    for tag in tags:
        categories_map.setdefault(tag.category, []).append(tag.name)

    categories = [dict(category=category, tags=tags) for category, tags in categories_map.items()]
    return categories


@app.post("/activities/", response_model=schemas.Activity)
async def create_activity(activity: schemas.Activity, db: Session = Depends(get_db)):
    return crud.create_activity(db, activity)


@app.get("/activities/", response_model=List[schemas.Activity])
async def get_all_activities(db: Session = Depends(get_db)):
    return crud.get_all_activities(db)

@app.post("/mock/", response_model=schemas.User)
async def create_mock(db: Session = Depends(get_db)):
    return crud.create_mock(db)