import hashlib
import schemas
'''
import googlemaps
import geopy
import schemas
'''

def hash_password(password: str):
    hashed_password = hashlib.md5(password.encode()).hexdigest()
    return hashed_password

'''
class LocationHandler():
    def __init__(self):
        self.gmaps = googlemaps.Client(key='AIzaSyDjzU7LTSYzI0yZjIdeNaGw0t9-9NKluLs')

    
    def air_distance(self, loc1, loc2):
        return geodesic(loc1, loc2).km

    def get_coord(self, location: str):
        coord = self.gmaps.geocode(location)[0]['geometry']['location'] # get coordinates
        coord = (coord['lat'], coord['lng'])
        return coord

    def place_name_auto_complete(self, address: str):
        predictions = self.gmaps.places_autocomplete_query()
        return predictions[0]

    def get_distance_from_address_and_location(location: tuple(float), activity: schemas.Activity):
        return self.air_distance(self.get_coord(activity.address), location)

    def sort_activities_by_distance(self, user: schemas.User, activities: list[schemas.Activity]):
        user_location = self.get_coord(user.address)
        return sorted(activities, key= lambda activity:self.get_distance_from_address_and_location(user_location, activity))

'''