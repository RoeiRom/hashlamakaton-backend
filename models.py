from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship

from database import Base


association_table = Table('association', Base.metadata,
    Column('user_id', ForeignKey('users.id'), primary_key=True),
    Column('tag_id', ForeignKey('tags.id'), primary_key=True)
)

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    hashed_password = Column(String)

    email = Column(String, unique=True)
    name = Column(String)
    age = Column(Integer)
    address = Column(String)
    gender = Column(Boolean)

    tags = relationship("Tag", secondary=association_table, backref="users")


class Tag(Base):
    __tablename__ = "tags"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    weight = Column(Integer)
    category = Column(String)
    user_id = Column(Integer, ForeignKey("users.id"))


class Activity(Base):
    __tablename__ = "activities"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    description = Column(String)
    host_name = Column(String)
    phone = Column(String)
    link = Column(String)
    address = Column(String)
    average_age = Column(Integer)
    image = Column(String)
