# from typing import List

from enum import Enum
from pydantic import BaseModel, Field

class Tag(BaseModel):
    name: str
    weight = 1
    category: str       # TODO: Make Enum

    class Config:
        orm_mode = True

class UserBase(BaseModel):
    username: str
    email: str

    name: str
    age: int
    address: str
    gender: bool
    tags: list[Tag] = []

class UserCreate(UserBase):
    password: str

class User(UserBase):
    id: int

    class Config:
        orm_mode = True

class UserCredntials(BaseModel):
    username: str
    password: str

class Activity(BaseModel):
    name: str
    description: str
    host_name: str
    phone: str
    link: str
    address: str
    average_age: int
    image: str
    # users = List[User] = []
    # tags = List[Tag] = []

    class Config:
        orm_mode = True
